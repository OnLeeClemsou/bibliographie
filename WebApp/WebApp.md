# Définition d'une Web App

Une Web App est une application, **mais utilisable sur internet**. À l’inverse d’une application traditionnelle qui nécessite une installation sur un poste et s’utilise en local, une Web App est hébergée sur un serveur et peut être accessible et manipulable depuis n’importe quel poste disposant d’une connexion internet, **sans installation logicielle**.

Il y a de plus en plus de Web App qui sont développées au fils des années et beaucoup prennent la place d'application natives. Nous allons voir pourquoi il y a un engoument soudain pour les Web App et quels sont leurs force.

# Avantages d'une Web App : 

 - Elle peuvent s’utiliser sans configuration.
 - Elle se mettent a jour dynamiquement (L’utilisateur n’a pas besoin d’installer de mise a jour).
 - Elles sont facile d’accès car utilisable sur la plupart des navigateurs.
 - Le travail en collaboration est grandement facilité.
 - Elle sont modulables et interconnectées (l’utilisateur peut personnaliser l’application selon son propre besoin).
 - On peut les faire évoluer comme on le souhaite (pas de restriction technique lié à l'OS).
 - C'est plus économique de développer une Web App.

 # Inconvénients : 

 - Il faut bien penser à la developper pour n'importe quel support (Navigateurs, taille sur téléphone ou tablette...).
 - Il faut payer peut être plus de serveur pour les traitements de données et l'host de l'application.
 - Si le débit n'est pas bon et que vous essayez d'utiliser l'application, cela peut prendre du temps voir même ne pas marcher.
 - Il sera plus compliqué d'intéragir avec le client (certaines fonctions seront difficiles à implémenter, voir impossible);

 # Conclusion

 Les Web App sont pratiques, peu cher à développer et permettent une grande accèsibilité.
