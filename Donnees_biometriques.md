# Données biométriques 

<img src="https://www.editions-legislatives.fr/media/news-npa/resized/650/467415-adobestock_190928452.jpeg">

## Qu'est-ce que c'est ?

> Les données biométriques sont les caractéristiques physiques strictement propres à un individu. 

Voici une liste des données biométriques souvent récoltées : 

- L'iris
- La voix
- Le visage
- Les empreintes digitales

---

## Quels sont les risques

Une fois les données en leur possession, les hackers peuvent s’en servir à des fins d’usurpation d’identité. Un moule de vos empreintes digitales peut par exemple servir à déverrouiller un appareil électronique qui vous appartient.

Un autre problème des données biométriques est justement que certaines d’entre elles peuvent être facilement dupliquées. Par exemple, il est possible de récupérer vos empreintes digitales sur un verre que vous avez touché dans un bar.

De plus, une fuite de données biométriques est bien plus compromettante qu’une fuite de données "classiques" . En effet, s’il est possible de changer un mot de passe après s’être fait pirater, il est impossible de modifier ses empreintes digitales ou son iris. Une fois ces données dérobées, votre sécurité est mise en péril pour toujours.

---

## Comment les protéger ? 

Tout d’abord, tâchez de limiter l’usage de vos données biométriques afin qu’elles ne soient stockées que dans un faible nombre de bases de données. Lorsque vous avez le choix entre une authentification par mot de passe ou par biométrie, il peut être préférable d’opter pour un bon vieux mot de passe.

Dans la mesure du possible, assurez-vous aussi que les bases de données contenant vos données biométriques appartiennent à des entreprises dignes de confiance. Par exemple, dans le cas des smartphones, vous ne pouvez pas accorder le même degré de confiance à une entreprise comme Apple qu’à un obscur constructeur asiatique méconnu du grand public…

Sources : 

1. [lebigdata.fr](https://www.lebigdata.fr/donnees-biometriques-definition-securite)