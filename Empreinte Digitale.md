# Empreinte digitale

## Définition

L'empreinte digitale est une image d'une doigt sur une surface plate. Les empreintes digitales sont uniques à chaque individu et chaque doigt a son empreinte propre. La probabilité que deux personnes aient les mêmes empreintes digitales est pratiquement impossible : une chance sur 64 milliards. 

## L'apparition des empreintes digitale sur smartphone

Un Smartphone n’est jamais assez sécurisé pour la plupart des gens. Code numérique, pattern, mot de passe en tout genre semblent ne pas suffire pour un verrouillage optimal du Smartphone. À l’ère de l’évolution technologique, le verrouillage du Smartphone grâce à l’empreinte digitale est apparu et a révolutionnée la connexion rapide et sécurisée en 2011.

## Explication de la technologie

Un scanner d'empreinte digitale capacitif est composé d'une multitude de condensateurs très sensibles aux changements de charge électrique. Lorsque l'utilisateur place son doigt sur le scanner, les différents niveaux de charge permettent de créer une image virtuelle de son empreinte digitale à part de sa structure physique. Il existe également des scanners optiques, beaucoup moins fiables, qui peuvent être trompés par une photo de l'empreinte digitale.

## Les avantages de la connexion par l'empreinte digitale

- Connexion rapide et sécurisé

## Les inconvénients de la connexion par l'empreinte digitale

- Stockage des informations biométrique dans le smartphone

