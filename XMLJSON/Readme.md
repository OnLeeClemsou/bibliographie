1. JSON

Qu'est-ce que le langage JSON en informatique ? 

- JSON (JavaScript Objet Notation) est un langage léger d’échange de données textuelles. 
- Pour les ordinateurs, ce format se génère et s’analyse facilement. 
- Pour les humains, il est pratique à écrire et à lire grâce à une syntaxe simple et à une structure en arborescence. 
- JSON permet de représenter des données structurées (comme XML par exemple).

Comment fonctionne le JSON ? 

- Fondé sur un sous-ensemble de Javascript, JSON est un format texte totalement indépendant de tout langage.
- Pourtant, les conventions utilisées ne surprendront pas les codeurs familiers aux langages descendant du C tels que Javascript, Python, Pearl ou d’autres.
- Autrement dit, il fonctionne un peu comme le XML (mais en moins développé) et facilite la structuration des informations présentes dans un document informatique. 
- Comme il sert simplement à fluidifier l'échange de données, il n'est pas supposé contenir de commentaires, par exemple, ce qui le distingue d'un langage informatique à part entière. 
- Toutefois, certaines bibliothèques en acceptent, s'ils sont écrits en JavaScript.
- Le JSON fait partie des langages compréhensibles aussi bien par un esprit humain que par une machine. 
- D'ailleurs, son apprentissage est facile et intuitif. 
- Cependant, il reste très limité et ce qui le rend moins fiable et peu résistant en termes de sécurité. 


Quelles sont les principales utilisations du langage JSON ?

- Les requêtes AJAX, car son exploitation par Javascript est plus rapide et plus simple que par XML.
- Envoyer des informations, depuis un serveur vers un utilisateur, afin de les afficher sur une page web, ou inversement. Ces caractéristiques en font un langage d’échange de données idéal et universel.
- Format de texte pour implémenter les balises de données structurées schema.org, qui structurent les pages HTML et permettent aux moteur de recherche de comprendre leur contenu. Actuellement, ce format est d'ailleurs recommandé par Google. 


2. XML

Qu'est-ce que le langage XML ?

- Le XML, pour Extensible Markup Language, désigne un langage informatique (ou métalangage pour être plus précis) utilisé, entre autres, dans la conception des sites Web et pour faciliter les échanges d'informations sur Internet. Ce langage de description a pour mission de formaliser des données textuelles. Il s'agit, en quelque sorte, d'une version améliorée du langage HTML avec la création illimitée de nouvelles balises. 
- Comme le langage HTML, le XML permet la mise en forme de documents via l'utilisation de balises. Développé et standardisé par le World Wide Web Consortium à la fin des années 1990, il répondait à l'objectif de définition d'un langage simple, facile à mettre en application.
- Le XML se classe dans la catégorie des langages de description (il n'est ni un langage de programmation, ni un langage de requêtes). Il est donc naturellement utilisé pour décrire des données en s'appuyant sur des balises et des règles personnalisables.
- Avec la généralisation de la connexion HTTP, le XML a encore gagné en popularité en devenant une solution habituelle pour créer un nouveau protocole.

Caractéristiques du langage XML

- Contrairement à d'autres, le XML ne demande pas de connaissances techniques en codage pour être utilisé. Comme il est naturellement structuré, il est facile à lire et à comprendre. 
- Ce langage est également très accessible : pas de besoin d'un logiciel d'édition de code pour l'écrire. Un logiciel de traitement de texte basique comme le bloc-note fait l'affaire. Il est universel, c'est-à-dire qu'il ne rencontre pas de problème de compatibilité avec d'autres technologies. Enfin, il est extensible, puisque il est toujours possible d'ajouter de nouvelles balises au fil des besoins. 

Comment fonctionne un fichier XML ? 

- La syntaxe du XML repose sur une chaîne de caractères structurée en deux niveaux : un pour le lecteur humain et un autre pour la machine. un document XML prend la forme d'un arbre, dont le tronc sert de support à différents types d'éléments appelés "noeuds", comme des textes, attributs, commentaires, éléments, etc
- Les débutants sont parfois découragés parce que la moindre erreur dans la forme fait sortir le document du format XML et casse toute la chaîne de traitement. 

Traduction du mot XML

- Langage de balisage extensible
- Le langage de balisage extensible (XML) est un langage informatique permettant de mettre en forme un document avec des balises.
- Extensible markup language (XML) is a computer language used for configuring documents using tags.

